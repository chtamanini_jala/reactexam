import React from "react";

function CalendarTasksContainer(){
    return(
        <div>
            <h3>Tasks</h3>
            <div className="calendar-tasks-container">
                <div className="calendar-tasks-item">
                    Project Tasks
                </div>
                <div className="calendar-tasks-item">
                    Personal Tasks
                </div>
                <div className="calendar-tasks-item">
                    IDK
                </div>
            </div>
            <div className="calendar-tasks-container">
                <a className="calendar-tasks-btn">+ Create Task</a>
                <a className="calendar-tasks-btn">My Calendar</a>
            </div>
        </div>
    );
}

export default CalendarTasksContainer;