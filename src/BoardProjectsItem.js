import React from "react";
import PropTypes from "prop-types";

function BoardProjectsItem({name, qty}){

    return(
        <div className="board-project-grid-item-container">
            <div className="test-div">
                <div className="test-div-s"><h3>{name}</h3></div>
                <div className="test-div-s">
                    <div className="test-div-s-s">Images</div>
                    <div className="test-div-s-s">{qty}/100</div>
                </div>
                <div className="test-div-s">
                    <div className="test-div-s-s">User Role</div>
                    <div className="test-div-s-s"><button className="btn-star">*</button></div>
                </div>
            </div>
        </div>
    );
}

BoardProjectsItem.propTypes ={
    name: PropTypes.string.isRequired,
    qty: PropTypes.number.isRequired,
}

export default BoardProjectsItem;