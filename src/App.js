import './App.css';
import ProjectContainer from './ProjectContainer';

const PROJECTS = [
  {
    name:"Project-1",
    quantity:7
  },
  {
    name:"Project-2",
    quantity:3
  },
  {
    name:"Project-3",
    quantity:15
  },
  {
    name:"Project-4",
    quantity:78
  },
  {
    name:"Project-5",
    quantity:8
  },
  {
    name:"Project-6",
    quantity:10
  },
];

function App() {
  return (
    <div className="main-container">
        <ProjectContainer projects={PROJECTS}/>
    </div>
  );
}

export default App;
